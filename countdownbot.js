var request = require('request');

module.exports = function (req, res, next) {
	var user = {name: req.body.user_name, id: req.body.user_id};
	var channel_id = req.body.channel_id;
	var text = req.body.text;
	var token = req.body.token;
	var botPayload = {};

	if (token !== process.env.SLASH_COMMAND_TOKEN) {
		return next(new Error('Invalid Command Token'));
	}

	botPayload.text = user.name + ' started a countdown:\n&gt;' + text;
	botPayload.username = BOT_NAME;
	botPayload.channel = channel_id;
	//botPayload.icon_emoji = BOT_ICON_EMOJI;

	console.log('Sending countdown request');

	send(botPayload, function (error, status, body) {
		if (error) {
			return next(error);
		} else if (status !== 200) {
			return next(new Error('Incoming WebHook: ' + status + ' ' + body));
		} else {
			var countdown = new SlackCountdown(text, user, channel_id);
			if (countdown.startCountdown() !== false){
				return res.status(200).end();
			} else {
				return next(new Error('Failed to start the countdown'));
			}			
		}
	});
}

var BOT_NAME = 'Countdown Bot';
var BOT_ICON_EMOJI = ':clock3:';

function SlackCountdown(text, user, channel_id) {
	this.text = text;
	this.user = user;
	this.channel_id = channel_id;
	this.increments = [10, 5, 1, 0]; // default message times

	var time = extractTime(text);
	if (time) {
		this.totalTime = time[0];
		this.timeUnit = time[1];
		this.timeRemaining = this.totalTime;
	}
}

SlackCountdown.prototype.startCountdown = function () {
	if (this.totalTime === undefined || this.timeUnit === undefined){
		return false;
	} else {
		this.scheduleNextIncrement();
	}
}

SlackCountdown.prototype.scheduleNextIncrement = function () {
	console.log('Scheduling next increment');
	var increment;
	do {
		increment = this.increments.shift();
	} while (increment > this.timeRemaining)

	console.log('Increment is ' + increment);
	var that = this;
	if (increment > 0) {
		setTimeout(function () {
		 		that.timeRemaining = increment;
		 		that.displayCountdown();
		 		that.scheduleNextIncrement();
			},
			(this.timeRemaining - increment) * (times[this.timeUnit])
		);
	} else {
		setTimeout(function() { that.finish(); }, this.timeRemaining * (times[this.timeUnit]));
	}
}

SlackCountdown.prototype.displayCountdown = function () {
	var botPayload = {};
	//botPayload.icon_emoji = BOT_ICON_EMOJI;
	botPayload.username = BOT_NAME;
	botPayload.channel = this.channel_id;

	var timeTextMatches = this.text.match(pattern);
	timeText = timeTextMatches[0].replace(timeTextMatches[1], this.timeRemaining);

	if (this.timeUnit.charAt(this.timeUnit.length - 1) === 's' && this.timeRemaining === 1) {
		timeText = timeText.replace(timeTextMatches[2], this.timeUnit.slice(0, -1));
	}

	botPayload.text = this.text.replace(pattern, timeText);

	send(botPayload, function (error, status, body) {
		if (error) {
			return next(error);
		} else if (status !== 200) {
			return next(new Error('Incoming WebHook: ' + status + ' ' + body));
		}
	});
}

SlackCountdown.prototype.finish = function () {
	var botPayload = {};
	//botPayload.icon_emoji = BOT_ICON_EMOJI;
	botPayload.username = BOT_NAME;
	botPayload.channel = this.channel_id;

	var indexOfIn = this.text.indexOf(' in ');
	botPayload.text = 
		this.text.substr(0, indexOfIn > 0 ? indexOfIn : this.text.length);

	send(botPayload, function (error, status, body) {
		if (error) {
			return next(error);
		} else if (status !== 200) {
			return next(new Error('Incoming WebHook: ' + status + ' ' + body));
		}
	});

	var userNotificationPayload = {};
	userNotificationPayload.icon_emoji = BOT_ICON_EMOJI;
	userNotificationPayload.username = BOT_NAME;
	userNotificationPayload.channel = '@' + this.user.name;
	userNotificationPayload.text = 'Your countdown has finished.\n&gt;' + this.text;

	send(userNotificationPayload, function (error, status, body) {
		if (error) {
			return next(error);
		} else if (status !== 200) {
			return next(new Error('Incoming WebHook: ' + status + ' ' + body));
		}
	});
}

function send(payload, callback) {
	var path = process.env.INCOMING_WEBHOOK_PATH;
	var uri = 'https://hooks.slack.com/services' + path;

	request({
		uri: uri,
		method: 'POST',
		body: JSON.stringify(payload)
	}, function (error, response, body) {
		if (error) {
			return callback(error);
		}

		callback(null, response.statusCode, body);
	});
}


/*
  Time map
  borrowed from https://github.com/popomore/schedule/blob/master/index.js
*/
var pattern = /\s*(\d+)(?:$|\s*((?:min(?:ute)?|second|hour)s?)\s*$)/

var times = {
  second: 1000,
  minute: 60000,
  min: 60000,
  hour: 3600000,
};

for (var key in times) {
  times[key + 's'] = times[key];
}

function extractTime(str) {
  var m = str.match(pattern);
  if (m && times[m[2]]) {
    return m.slice(1);
  } else if (m[1]){
  	return [m[1], 'minutes'];
  }
  return null;
}