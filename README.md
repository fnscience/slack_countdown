# Slack Countdown

A simple Slack integration to show countdown messages.

## Usage
`/countdown [action] in [n] [time unit]`
`[action]` any text descrbing what you intend to do when the countdown finishes
`[n]` how long the countdown last
`[time unit]` the unit of time the countdown is measured in, typically minutes
 
Example: `/countdown Deploying UI in 10 minutes`

 _Text may also appear between 'in' and `[n]` in the command_

## Notes
Currently supports hour, minute and second time units. Note that when using
seconds, messages may appear out of order due to the asynchronous nature of
Slack's Incoming Web Hook integration.

Slack will display countdown messages at 10, 5, and 1 time units remaining. At
the end of the countdown, the `[action]` text will be displayed. For the
example above, Slack will display "Deploying UI" when the countdown ends. A
direct message is also sent to the user who created the countdown to notify
him/her that their countdown has finished.